__module_author__ = 'Aww'
__module_name__ = 'Notes'
__module_version__ = '1.0.1'
__module_description__ = 'Make notes of users'
#########################
# Commands:
# /notes add <nick> <note>
# /notes del <id>
# /notes list
#########################
import sqlite3, xchat

## Create and or load the notes database file.
conn = sqlite3.connect("%s/notes.db" % xchat.get_info("xchatdir"))
## Create a notes table if it doesn't already exist.
conn.execute("CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY, nick TEXT, note TEXT);")

def notes(word, word_eol, userdata):
    if word[1] == "add":
        if len(word) > 2:
            ## Add the note to the notes database.
            conn.execute("INSERT INTO notes (nick, note) VALUES ('%s', '%s')" % (word[2], word_eol[3]))
            ## Commit it to the database
            conn.commit()
            ## Fake a channel event to let you know it worked.
            xchat.emit_print("Channel Message", "&Notes", "Added a new note for %s" % word[2])
        else:
            ## Fake two channel events to let you know the correct format
            xchat.emit_print("Channel Message", "&Notes", "Format for 'add' /notes add <nick> <note>", "")
            xchat.emit_print("Channel Message", "&Notes", "Example: /notes add Aww is an awesome person!", "")
    elif word[1] == "list":
        notes = conn.execute("SELECT * FROM notes")
        for note in notes:
            xchat.emit_print("Channel Message", "&Notes", "ID: %s Nick: %s Note: %s" % note, "")
    elif word[1] == "read":
        if len(word) > 1:
            notes = conn.execute("SELECT * FROM notes")
            for id, nick, note in notes:
                if nick == word[2]:
                    xchat.emit_print("Channel Message", "&Notes", "Nick %s: %s" % (nick, note), "")
        else:
            ## Fake two channel events to let you know the correct format
            xchat.emit_print("Channel Message", "&Notes", "Format for 'add' /notes read <nick>", "")
            xchat.emit_print("Channel Message", "&Notes", "Example: /notes read Aww", "") 
    elif word[1] == "del":
        if len(word) > 1:
            conn.execute("DELETE FROM notes WHERE ID = %s" % word[2])
            conn.commit()
        else:
            ## Fake two channel events to let you know the correct format
            xchat.emit_print("Channel Message", "&Notes", "Format for 'add' /notes del <id>", "")
            xchat.emit_print("Channel Message", "&Notes", "Example: /notes del 2", "") 
                
## hook into xchat
xchat.hook_command("notes", notes)
            
